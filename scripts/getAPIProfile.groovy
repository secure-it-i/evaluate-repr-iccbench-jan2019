/*
 * Copyright (c) 2018, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Authors: Venkatesh-Prasad Ranganath
 *         Joydeep Mitra
 */


import groovy.transform.Memoized
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.FieldVisitor
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type
import org.ow2.asmdex.ApplicationReader
import org.ow2.asmdex.ApplicationVisitor
import org.ow2.asmdex.ClassVisitor as DexClassVisitor
import org.ow2.asmdex.MethodVisitor as DexMethodVisitor
import org.ow2.asmdex.FieldVisitor as DexFieldVisitor

import java.nio.file.FileVisitOption
import java.nio.file.Files
import java.nio.file.Paths
import java.util.jar.JarFile
import java.util.zip.ZipFile


// TODO: Change this regex to control android platforms to consider for analysis
consideredAndroidPlatform = 'android-25'


def cli = new CliBuilder(usage:"groovy getAPIProfile.groovy")
cli.f(longOpt:'folder', args:1, argName:'folder', required:true,
        'Folder containing APKs to analyze')
cli.a(longOpt:'androidHome', args:1, argName:'androidHome', required:true,
        'Absolute path to Android sdk folder')
cli.p(longOpt:'profileFile', args:1, argName:'profileFile', required:true,
        'Name of output profile file')
cli.i(longOpt:'idFile', args:1, argName:'idFile', required:true,
        'Name of output id to api mapping file')
def options = cli.parse(args)
if (!options) {
    return
}

def androidHome = options.a
CHA androidCHA = new CHA()
Files.find(Paths.get("$androidHome/platforms/" + consideredAndroidPlatform), 5,
        { p, _ -> p.getName(p.nameCount - 1).toString().endsWith('jar') }, FileVisitOption.FOLLOW_LINKS)
    .forEach {
        println "Processing $it"
        def jarFile = new JarFile(it.toFile())
        jarFile.stream().filter { it.name ==~ /.*class/ } .forEach { entry ->
            androidCHA.process(jarFile.getInputStream(entry))
        }
    }

new File(options.p).withPrintWriter { def writer ->
    writer.println("APK,TargetSDK,MinSDK,APIs Features")
    def files = []
    new File(options.f).eachFileMatch { it.endsWith(".apk") } { files << it }
    files.parallelStream().map { File file ->
        try {
            def manifestData = DataGatherer.getManifestData(file, androidHome)
            def packageData = DataGatherer.getPackageData(file, androidCHA)
            println "Analyzed $file"

            if (!manifestData.isEmpty() && !packageData.isEmpty()) {
                def ret = ["${file.getName()}"]
                ret += manifestData
                ret += packageData
                Optional.of(ret)
            } else
                Optional.empty()
        } catch (e) {
            println "Exception while processing $file: ${e.message}"
            e.printStackTrace(System.out)
            Optional.empty()
        }
    }.forEach {
        it.ifPresent { v ->
            def tmp1 = v.join(',')
            synchronized(writer) {
                writer.println(tmp1)
            }
        }
    }
}

DataGatherer.writeId2API(options.i)


class DataGatherer {
    static Map<String, Integer> api2id = { ->
        def t = new HashMap<String, Integer>()
        return t.withDefault { k -> t.size() + 1 }.asSynchronized()
    }()

    static writeId2API(outFile) {
        new File(outFile).withPrintWriter { writer -> api2id.each { k, v -> writer.println("$v!$k") } }
        println api2id.size()
    }

    static List<String> slurpElementPresence(slurper, String element) {
        slurper.'**'.findAll { it.name() == element }.collect { "E,$element" }
    }

    static List<String> slurpAttributePresence(slurper, String attribute, List<String> elements) {
        def tmp1 = "@$attribute".toString()
        slurper.'**'.findAll { it.name() in elements && !it[tmp1].isEmpty() }
                .collect { "A,${it.name()},$attribute" }
    }

    static List<String> slurpAttributeValue(slurper, String attribute, List<String> elements) {
        def tmp1 = "@$attribute".toString()
        slurper.'**'.findAll { it.name() in elements && !it[tmp1].isEmpty() }
                .collect { "A,${it.name()},$attribute=${it[tmp1].text()}" }
    }

    static getManifestData(apk, androidHome) {
        def out = new StringBuilder()
        def err = new StringBuilder()
        def process = "$androidHome/tools/bin/apkanalyzer manifest print $apk".execute()
        process.waitForProcessOutput(out, err)

        def ret = []
        if (err.size() > 0) {
            println "Error while retrieving manifest from $apk"
            println err
        } else if (out.size() > 0) {
            try {
                def pathResult = new XmlSlurper(false, true).parseText(out.readLines().join(''))

                def sdks = pathResult.'**'.findAll { it.name() == 'uses-sdk' }.collect {
                    def minSdk = it.'@android:minSdkVersion'[0].text()
                    def tmp2 = it.'@android:targetSdkVersion'
                    def targetSdk = tmp2.isEmpty() ? minSdk : tmp2[0].text()
                    [targetSdk, minSdk]
                }.unique().sort { -(it[0] as int) }
                ret.addAll(sdks.size() == 0 ? ['-1', '-1'] : sdks[0])  // -1 indicates default SDK version was injected

                def values = pathResult.'**'.findAll { it.name() == 'intent-filter' }.collect {
                    it.action.collect { "A,intent-filter,action,${it.'@android,name'.text()}" } +
                            it.category.collect { "A,intent-filter,category,${it.'@android,name'.text()}" }
                }
                values << slurpAttributeValue(pathResult, 'android:protectionLevel', ['permission'])
                values << slurpAttributeValue(pathResult, 'android:name', ['uses-library'])
                values << slurpAttributeValue(pathResult, 'android:name', ['uses-permission'])
                values << slurpAttributeValue(pathResult, 'android:name', ['uses-permission-sdk-23'])
                values << slurpAttributeValue(pathResult, 'android:exported', ['uses-permission-sdk-23'])
                ret.addAll(values.collectMany { it }.collect { api2id.get(it) })

                def presences = [slurpElementPresence(pathResult, 'action')]
                presences << slurpElementPresence(pathResult, 'category')
                presences << slurpElementPresence(pathResult, 'uses-permission')
                presences << slurpElementPresence(pathResult, 'permission-tree')
                presences << slurpElementPresence(pathResult, 'uses-library')
                presences << slurpElementPresence(pathResult, 'grant-uri-permission')
                presences << slurpAttributePresence(pathResult, 'android:protectionLevel', ['permission'])
                presences << slurpAttributePresence(pathResult, 'android:allowTaskReparenting', ['activity',
                                                                                                 'application'])
                presences << slurpAttributePresence(pathResult, 'android:launchMode', ['activity'])
                presences << slurpAttributePresence(pathResult, 'android:permission', ['activity', 'service',
                                                                                       'receiver', 'provider'])
                presences << slurpAttributePresence(pathResult, 'android:taskAffinity', ['activity', 'application'])
                presences << slurpAttributePresence(pathResult, 'android:allowBackup', ['application'])
                presences << slurpAttributePresence(pathResult, 'android:debuggable', ['application'])
                presences << slurpAttributePresence(pathResult, 'android:scheme', ['data'])
                presences << slurpAttributePresence(pathResult, 'android:path', ['data', 'uses-permission'])
                presences << slurpAttributePresence(pathResult, 'android:pathPrefix', ['data', 'uses-permission'])
                presences << slurpAttributePresence(pathResult, 'android:priority', ['intent-filter'])
                presences << slurpAttributePresence(pathResult, 'android:readPermission', ['path-permission',
                                                                                           'provider'])
                presences << slurpAttributePresence(pathResult, 'android:writePermission', ['path-permission',
                                                                                            'provider'])
                presences << slurpAttributePresence(pathResult, 'android:exported', ['activity', 'service', 'receiver',
                                                                                     'provider'])
                ret.addAll(presences.collectMany { it }.collect { api2id.get(it) })
            } catch (Exception e) {
                println "Exception $e while processing manifest of $apk: ${e.message}"
                e.printStackTrace(System.out)
                ret.clear()
            }
        } else {
            println "Error: No output was generated for $apk"
        }
        ret.unique()
    }

    static getPackageData(apk, CHA androidCHA) {
        def ret = [] as Set
        def zipFile = new ZipFile(apk)
        def entries = zipFile.entries().findAll { it.getName() ==~ /.*dex$/ }
        if (entries.empty)
            println "Error while retrieving dex file from $apk"
        else {
            def appCHA = new CHA(androidCHA)
            def appVisitor = new MyAppVisitor(Opcodes.ASM4, appCHA)
            entries.each { entry->
                new ApplicationReader(Opcodes.ASM4,
                    zipFile.getInputStream(entry)).accept(appVisitor, 0)
            }

            def transform = { members ->
                members.collectMany { m ->
                    def member = m.collect{ it.toString() }
                    appCHA.getMemberIntroducingTypeNames(member[1], member[2], member[3]).findAll {
                        assert it instanceof String
                        !(it ==~ /^android\/support.*/) && MyAppVisitor.shouldConsiderNonAppClass(it)
                    }.collect { [member[0], it, member[2], member[3]].join(',') }
                }
            }
            def publishedDefs = transform(appVisitor.definedPublishedAppMembers) as Set
            def allDefs = transform(appVisitor.getAllDefinedMembers()) as Set
            def allRefs = transform(appVisitor.referencedMembers.findAll { it[1][0] != '[' }) as Set
            ret.addAll(publishedDefs)
            ret.addAll(allRefs - allDefs)
        }
        ret.collect { api2id.get(it) }.unique()
    }
}


class CHA {
    def className2superclassName = [:]
    def className2interfaceNames = [:]
    def className2memberNames2descriptions = [:].withDefault { [:].withDefault { [] as Set } }

    CHA(CHA parent = null) {
        if (parent) {
            this.className2superclassName.putAll(parent.className2superclassName)
            parent.className2interfaceNames.each { k, v -> this.className2interfaceNames[k] =  v.collect() }
            parent.className2memberNames2descriptions.each { kp1, vp1 ->
                def tmp1 = this.className2memberNames2descriptions[kp1]
                vp1.each { kp2, vp2 -> tmp1[kp2].addAll(vp2) }
            }
        }
    }

    def process(InputStream inputStream) {
        def cr = new ClassReader(inputStream)
        cr.accept(new CHAClassVisitor(Opcodes.ASM6),
                ClassReader.SKIP_CODE|ClassReader.SKIP_DEBUG|ClassReader.SKIP_FRAMES)
    }

    String getSuperClass(String internalClassName) {
        return className2superclassName[internalClassName]
    }

    Collection<String> getInterfaces(String internalClassName) {
        return className2interfaceNames[internalClassName]
    }

    def recordClass(String internalName, String superInternalName, Collection<String> interfacesInternalNames) {
        className2superclassName[internalName] = superInternalName
        className2interfaceNames[internalName] = interfacesInternalNames as Set
    }

    def recordMember(String ownerInternalName, String name, String desc) {
        className2memberNames2descriptions[ownerInternalName][name].add(desc)
    }

    boolean wasClassAnalyzed(String internalName) {
        return internalName in className2superclassName
    }

    Collection<String> getDescriptions(String internalClassName, String memberName) {
        return className2memberNames2descriptions[internalClassName][memberName]
    }


    @Memoized(maxCacheSize = 100000)
    def getMemberIntroducingTypeNames(String internalClassName, String memberName, String desc) {
        def ret = []
        if (memberName.startsWith('<'))
            ret << internalClassName
        else if (wasClassAnalyzed(internalClassName)) {
            def interfaces = getInterfaces(internalClassName)
            if (interfaces)
                ret.addAll(interfaces.collectMany { getMemberIntroducingTypeNames(it, memberName, desc) })

            def superclassName = getSuperClass(internalClassName)
            if (superclassName)
                ret.addAll(getMemberIntroducingTypeNames(superclassName, memberName, desc))

            if (!ret && desc in getDescriptions(internalClassName, memberName))
                ret << internalClassName
        }

        return ret as Set
    }

    @Memoized(maxCacheSize = 100000)
    def getMemberDefiningTypeNames(String internalClassName, String memberName, String desc) {
        def ret = []
        if (memberName.startsWith('<'))
            ret << internalClassName
        else if (wasClassAnalyzed(internalClassName)) {
            if (desc in getDescriptions(internalClassName, memberName))
                ret << internalClassName
            else {
                def superclassName = getSuperClass(internalClassName)

                if (superclassName) {
                    ret.addAll(getMemberDefiningTypeNames(superclassName, memberName, desc))
                }

                Collection<String> interfaces = getInterfaces(internalClassName)
                if (interfaces)
                    ret.addAll(interfaces.collectMany { getMemberDefiningTypeNames(it, memberName, desc) })
            }
        }
        return ret as Set
    }

    private class CHAClassVisitor extends ClassVisitor {
        String className
        CHAClassVisitor(int api) {
            super(api)
        }

        void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            this.className = Type.getType(name).getInternalName()
            recordClass(this.className, superName ? Type.getType(superName).getInternalName() : null,
                    interfaces.collect { Type.getType(it).getInternalName() })
        }

        FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
            recordMember(this.className, name, desc)
            return null
        }

        MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
            recordMember(this.className, name, desc)
            return null
        }
    }
}


class MyAppVisitor extends ApplicationVisitor {
    public definedPublishedAppMembers = [] as Set
    private definedUnpublishedAppMembers = [] as Set
    private definedNonAppMembers = [] as Set
    private referencedMembers = [] as Set
    private cha

    MyAppVisitor(int api, CHA cha) {
        super(api)
        this.cha = cha
    }

    DexClassVisitor visitClass(int access, String name, String[] signature, String superName, String[] interfaces) {
        String internalName = Type.getType(name).getInternalName()
        cha.recordClass(internalName, Type.getType(superName).getInternalName(),
                interfaces.collect { String it -> Type.getType(it).getInternalName() })
        new MyClassVisitor(this.api, internalName)
    }

    static def isObfuscated(String name) {
        name.split('/').any { String it -> it.length() == 1 }
    }

    static def shouldConsiderNonAppClass(String internalClassName) {
        internalClassName ==~ /^((android)|(java)|(org)|(com.android)).*/
    }

    static def fixMethodDesc(String desc) {
        def idx = desc[0] != 'L' ? 1 : desc.indexOf(';') + 1
        def descSize = desc.size()
        def ret = idx == descSize ? desc : "(${desc[idx..-1]})${desc[0..<idx]}"
        (ret == desc ? "()$ret" : ret).toString()
    }

    def getAllDefinedMembers() {
        (this.definedNonAppMembers + this.definedPublishedAppMembers + this.definedUnpublishedAppMembers) as Set
    }

    private class MyClassVisitor extends DexClassVisitor {
        private String className

        MyClassVisitor(int api, String internalClassName) {
            super(api)
            this.className = internalClassName
        }

        DexMethodVisitor visitMethod(int access, String name, String desc, String[] signature, String[] exceptions) {
            if (!isObfuscated(this.className) && !isObfuscated(name)) {
                String fixedDesc = fixMethodDesc(desc)
                if (shouldConsiderNonAppClass(this.className))
                    definedNonAppMembers << ['M', this.className, name, fixedDesc]
                else {
                    def tmp1
                    if (access & (Opcodes.ACC_PROTECTED | Opcodes.ACC_PUBLIC))
                        tmp1 = definedPublishedAppMembers
                    else
                        tmp1 = definedUnpublishedAppMembers
                    tmp1 << ['M', this.className, name, fixedDesc]
                }
                cha.recordMember(this.className, name, fixedDesc)
            }

            new MyMethodVisitor(api)
        }

        DexFieldVisitor visitField(int access, String name, String desc, String[] signature, Object value) {
            cha.recordMember(this.className, name, desc)
            return null
        }
    }

    private class MyMethodVisitor extends DexMethodVisitor {

        MyMethodVisitor(int api) {
            super(api)
        }

        void visitFieldInsn(int opcode, String owner, String name, String desc, int valueRegister,
                            int objectRegister) {
            def ownerInternalName = Type.getType(owner).getInternalName()
            if (!isObfuscated(ownerInternalName) && !isObfuscated(name) &&
                    shouldConsiderNonAppClass(ownerInternalName)) {
                referencedMembers << ['F', ownerInternalName, name, desc]
                cha.recordMember(ownerInternalName, name, desc)
            }
        }

        void visitMethodInsn(int opcode, String owner, String name, String desc, int[] arguments) {
            if (!isObfuscated(owner) && !isObfuscated(name)) {
                String fixedDesc = fixMethodDesc(desc)
                def ownerInternalName = Type.getType(owner).getInternalName()
                referencedMembers << ['M', ownerInternalName, name, fixedDesc]
                cha.recordMember(ownerInternalName, name, fixedDesc)
            }
        }
    }
}
