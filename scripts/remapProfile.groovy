/*
 * Copyright (c) 2018, Venkatesh-Prasad Ranganath
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath
 */

import java.nio.file.Files
import java.nio.file.Paths
import java.util.logging.FileHandler

def cli = new CliBuilder(usage:"groovy remapProfile.groovy")
cli.t(longOpt:'target-ids', args:1, argName:'targetIds',
    required:true, 'Target Id to API mapping (e.g., from Ghera APKs)')
cli.s(longOpt:'source-ids', args:1, argName:'sourceIds',
    required:true, 'Source Id to API mapping (e.g., from AndroZoo APKs)')
cli.g(longOpt:'ignored-apis', args:1, argName:'ignoredApis',
    required:false, 'APIs to be ignored.')
cli.i(longOpt:'inputProfileFile', args:1, argName:'inputProfileFile',
    required:true, 'Name of source/input profile file')
cli.o(longOpt:'outputProfileFile', args:1, argName:'outputProfileFile',
    required:true, 'Name of output profile file (csv)')
def options = cli.parse(args)
if (!options) {
    return
}

def ignoredApis = [] as Set

if (options.g)
    ignoredApis.addAll(new File(options.g).readLines())

def api2id = new File(options.t).readLines().collect {
    it.split('!').reverse() as List
}.findAll { !(it[0] in ignoredApis) }.collectEntries()

def sourceId2Targetid = new File(options.s).readLines()
    .collect { it.split('!') }
    .findAll { it[1] in api2id }
    .collectEntries { [it[0], api2id[it[1]]] }

def targetIds = api2id.values().sort { it as Integer }
new File(options.o).withPrintWriter { writer ->
    writer.println('APKs,TargetSDK,MinSDK,' + targetIds.collect { "$it" }.join(','))

    Files.lines(Paths.get(options.i)).skip(1).parallel().map { line ->
        def tmp1 = line.split(',')
        def apk = tmp1[0]
        def targetSDK = tmp1[1]
        def minSDK = tmp1[2]
        def ids = tmp1.drop(3).findAll { it in sourceId2Targetid }
                .collect { sourceId2Targetid[it] }
        def tmp2 = targetIds.collect { it in ids ? '1' : '0' }.join(',')
        "$apk,$targetSDK,$minSDK,$tmp2"
    }.forEachOrdered { line ->
        synchronized(writer) {
            writer.println(line)
        }
    }
}
